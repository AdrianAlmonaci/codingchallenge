package com.example.nikealmonaci.model

data class WordSearched(
    val definition: String = "",
    val thumbs_up: Int = 0,
    val thumbs_down: Int = 0
)
