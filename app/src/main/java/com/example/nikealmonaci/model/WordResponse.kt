package com.example.nikealmonaci.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class WordResponse(
    @SerializedName("list")
    @Expose
    var list : List<WordSearched>
)

