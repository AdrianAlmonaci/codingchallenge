package com.example.nikealmonaci.network

import com.example.nikealmonaci.model.WordResponse
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface ApiService {

    @Headers(value = [
        "X-RapidAPI-Host: mashape-community-urban-dictionary.p.rapidapi.com",
        "X-RapidAPI-Key: 3d404503ccmsh1b444691a02639dp1a91fajsn2834fc71a48f"
    ]

    )
    @GET("/define")
    suspend fun getProperties(@Query("term") term: String):
            WordResponse

    companion object {
        private const val URL_URBAN_DICTIONARY = "https://mashape-community-urban-dictionary.p.rapidapi.com"
        fun  initService() : ApiService
        {
            return Retrofit.Builder()
                .baseUrl(URL_URBAN_DICTIONARY)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()
                .create(ApiService::class.java)
        }
    }

}