package com.example.nikealmonaci.util.components

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.nikealmonaci.model.WordSearched
import com.example.nikealmonaci.R
import kotlinx.android.synthetic.main.card_view.view.*

class RecyclerViewAdapter(val myData: MutableList<WordSearched>):
    RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder>() {

    class MyViewHolder(var myCardView: CardView): RecyclerView.ViewHolder(myCardView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        var myCardView = LayoutInflater.from(parent.context)
            .inflate(R.layout.card_view, parent, false) as CardView
        return MyViewHolder(myCardView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val myWordSearched = myData[position]
        holder.myCardView.cvTxt.text = myWordSearched.definition
        holder.myCardView.thumbs_up_txt.text = myWordSearched.thumbs_up.toString()
        holder.myCardView.thumbs_down_txt.text = myWordSearched.thumbs_down.toString()
    }

    override fun getItemCount() = myData.size
}