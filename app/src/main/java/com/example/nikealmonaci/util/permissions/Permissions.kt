package com.example.nikealmonaci.util.permissions

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.nikealmonaci.R
import java.util.jar.Manifest

class Permissions() {

    companion object Permission {
        fun checkPermission(activityPermission: Activity, permission: String) {
            val MY_PERMISSION_REQUESTED = 0

            if(ContextCompat.checkSelfPermission(activityPermission, permission)
                != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(activityPermission, arrayOf(permission),
                    MY_PERMISSION_REQUESTED)
            }
        }
    }
}