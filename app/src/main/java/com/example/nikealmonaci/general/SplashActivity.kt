package com.example.nikealmonaci.general

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.nikealmonaci.R
import com.example.nikealmonaci.ui.MainActivity

class SplashActivity : AppCompatActivity() {

    private val splashTime = 2000L //Two seconds
    private val myHandler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        setHandlerForSplashActivity()
    }

    private fun setHandlerForSplashActivity() {
        myHandler.postDelayed({goToMainActivity()}, splashTime)
    }

    private fun goToMainActivity(){
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}
