package com.example.nikealmonaci.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.nikealmonaci.model.WordSearched
import com.example.nikealmonaci.repository.Repository
import kotlinx.coroutines.*

class MainActivityViewModel: ViewModel() {

    companion object {
        const val THUMBS_UP = 0
        const val THUMBS_DOWN = 1
    }

    var liveDataResultList: MutableLiveData<MutableList<WordSearched>> = MutableLiveData()
    var resultList: MutableList<WordSearched> = listOf<WordSearched>().toMutableList()
    var editTxtWord: MutableLiveData<String> = MutableLiveData()
    var observableWord: MutableLiveData<String> = MutableLiveData()

    private val viewModelJob = SupervisorJob()
    private val viewModelScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    fun searchWord(word: String) {
        observableWord.value = editTxtWord.toString()
        viewModelScope.launch {
            getResultsFromWebService(word)
        }
    }

    fun startToSortList(listToSort: MutableList<WordSearched>, order: Int) {
        viewModelScope.launch {
            sortList(listToSort, order)
        }
    }

    private suspend fun sortList(listToSort: MutableList<WordSearched>, order: Int) = withContext(Dispatchers.Default){
        var listSorted = listToSort.sortedByDescending{
            when (order) {
                0 -> it.thumbs_up
                else -> it.thumbs_down
            }
        }
        updateList(listSorted.toMutableList())
    }

    private suspend fun updateList(listSorted: MutableList<WordSearched>) = withContext(Dispatchers.Default) {
        resultList.clear()
        resultList.addAll(listSorted)
        notifyLiveData()
    }

    private suspend fun notifyLiveData() = withContext(Dispatchers.Main) {
        liveDataResultList.value = resultList
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    private suspend fun getResultsFromWebService(word: String)  = withContext(Dispatchers.IO) {
        var myRepository =  Repository()
        updateList(myRepository.getList(word).list.toMutableList())
    }
}

