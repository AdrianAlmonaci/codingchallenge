package com.example.nikealmonaci.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.nikealmonaci.R
import com.example.nikealmonaci.databinding.ActivityMainBinding
import com.example.nikealmonaci.util.components.RecyclerViewAdapter
import com.example.nikealmonaci.util.permissions.Permissions
import kotlinx.android.synthetic.main.activity_main.*
import com.example.nikealmonaci.BR.mainActViewModel
import com.example.nikealmonaci.model.WordSearched
import com.example.nikealmonaci.util.components.KeyBoard

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var mainActivityViewModel: MainActivityViewModel
    private lateinit var listObserver: Observer<MutableList<WordSearched>>
    private lateinit var wordObserver: Observer<String>
    private lateinit var myAdapter: RecyclerViewAdapter
    private val keyBoard = KeyBoard()
    private val f = loaderDialogFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setDataBinding()
        checkPermissions()
        setObservers()
        setRecyclerView()
    }

    override fun onPause() {
        super.onPause()
    }

    private fun checkPermissions() {
        Permissions.checkPermission(this, android.Manifest.permission.INTERNET)
    }

    private fun setDataBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.lifecycleOwner = this
        mainActivityViewModel = ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
        binding.setVariable(mainActViewModel, mainActivityViewModel)
    }

    private fun setRecyclerView() {
        var r = recyclerView
        r.layoutManager = LinearLayoutManager(this)
        myAdapter = RecyclerViewAdapter(mainActivityViewModel.resultList)
        r.adapter = myAdapter
    }

    private fun setObservers() {
        listObserver = Observer {
            myAdapter.notifyDataSetChanged()
            keyBoard.hideSoftKeyboard(this)
            f.stopLoaderWidget()
        }
        mainActivityViewModel.liveDataResultList.observe(this, listObserver)

        wordObserver = Observer {
            setDialogFragment()
        }
        mainActivityViewModel.observableWord.observe(this, wordObserver)
    }

    fun setDialogFragment() {
        val t: FragmentTransaction = supportFragmentManager.beginTransaction()
        f.show(t,"")
    }
}
