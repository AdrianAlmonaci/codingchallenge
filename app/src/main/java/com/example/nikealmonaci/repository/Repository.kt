package com.example.nikealmonaci.repository

import com.example.nikealmonaci.network.ApiService
import retrofit2.Callback

class Repository {
    private val result = ApiService.initService()
    suspend fun getList(term: String) = result.getProperties(term)
}

